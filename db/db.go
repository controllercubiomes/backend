package db

import (
	"bytes"
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"time"

	sq "github.com/Masterminds/squirrel"
	_ "github.com/lib/pq"
	"gitlab.com/controllercubiomes/util"
)

type DBCred struct {
	DB_HOST     string
	DB_USER     string
	DB_PASSWORD string
	DB_NAME     string
}

type DB struct {
	Conn *sql.DB
}

type SeedResp struct {
	Seeds []util.Seed `json:"seeds"`
	Total int64       `json:"total"`
}

func (dbc DBCred) ConnectToDB() (*sql.DB, error) {
	dbinfo := ""
	if dbc.DB_PASSWORD == "" {
		dbinfo = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_NAME)
	} else {
		dbinfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_PASSWORD, dbc.DB_NAME)
	}

	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		return nil, err
	}

	return db, db.Ping()
}

func (db DB) GetVerifiedSeeds(page, limit uint64, fs util.Request) (sr SeedResp, err error) {

	selectSeeds := sq.Select(seedData).From("seeds")
	verifiedSeeds := selectSeeds.Where("verified = TRUE")
	countSeeds := sq.Select("count(*)").From("seeds")
	countSeeds = countSeeds.Where("verified = TRUE")
	query := []rune(fs.Filter)

	var tmp bytes.Buffer
	var stmt bytes.Buffer
	var tokens []util.Token
	for i := 0; i < len(query); i++ {
		tmp.WriteRune(query[i])
		switch t := tmp.String(); t {
		case "SUM(", "AVG(", "spawn_biome(":
			tokens = append(tokens, util.Token{Token: util.TOKEN_NAME, Val: t[:len(t)-1]})
			tokens = append(tokens, util.Token{Token: util.TOKEN_OPPERAND, Val: "("})
			tmp = bytes.Buffer{}

		case ">", "<", "=", "&&", "||", ",", ")":
			switch t {
			case ">", "<":
				if query[i+1] == '=' {
					i++
					tmp.WriteRune(query[i])
				}
			}
			tokens = append(tokens, util.Token{Token: util.TOKEN_OPPERAND, Val: tmp.String()})
			tmp = bytes.Buffer{}
		case " ":
			tmp = bytes.Buffer{}

		default:
			if len(query) == i+1 || query[i+1] == ' ' || query[i+1] == ',' || query[i+1] == ')' {
				tokens = append(tokens, util.Token{Token: util.TOKEN_VALUE, Val: tmp.String()})
				tmp = bytes.Buffer{}
			}
		}
	}

	for i := 0; i < len(tokens); i++ {
		switch tokens[i].Token {
		case util.TOKEN_NAME:
			val := tokens[i].Val
			i++
			if tokens[i].Token != util.TOKEN_OPPERAND || tokens[i].Val != "(" {
				log.Fatal("TOKEN_NAME not directly followed by ( opperand")
			}
			stmt.WriteRune('(')
			if val == "AVG" {
				stmt.WriteRune('(')
			}
			i++
			if tokens[i].Token != util.TOKEN_VALUE {
				log.Fatal("TOKEN_NAME did not have TOKEN_VALUE as secound TOKEN")
			}
			if val == "spawn_biome" {
				stmt.WriteString("spawn_biome = '")
			}
			stmt.WriteString(tokens[i].Val)
			if val == "spawn_biome" {
				stmt.WriteRune('\'')
			}
			i++
			count := 1
			for ; tokens[i].Token == util.TOKEN_OPPERAND; i++ {
				if tokens[i].Val == ")" {
					if val == "AVG" {
						stmt.WriteString(") / ")
						stmt.WriteString(strconv.Itoa(count))
					}
					stmt.WriteRune(')')
					break
				}
				if tokens[i].Val == "," {
					if val == "spawn_biome" {
						stmt.WriteString(" OR spawn_biome = '")
					} else {
						stmt.WriteString(" + ")
					}
					i++
					if tokens[i].Token != util.TOKEN_VALUE {
						log.Fatal("TOKEN_NAME not a sequence of oppr and value")
					}
					count++
					stmt.WriteString(tokens[i].Val)
					if val == "spawn_biome" {
						stmt.WriteRune('\'')
					}
				}
			}
		case util.TOKEN_OPPERAND:
			if tokens[i].Val != ")" {
				stmt.WriteRune(' ')
			}
			switch tokens[i].Val {
			case "||":
				stmt.WriteString("OR")
			case "&&":
				stmt.WriteString("AND")
			default:
				stmt.WriteString(tokens[i].Val)
			}
			stmt.WriteRune(' ')
		case util.TOKEN_VALUE:
			_, err := strconv.Atoi(tokens[i].Val)
			if i != 0 && err != nil && tokens[i-1].Token == util.TOKEN_OPPERAND {
				switch tokens[i-1].Val {
				case ">", ">=", "=", "<=", "<":
					stmt.WriteRune('\'')
					stmt.WriteString(tokens[i].Val)
					stmt.WriteRune('\'')
					continue
				}
			}
			stmt.WriteString(tokens[i].Val)
		}
	}
	if str := stmt.String(); len(str) > 0 {
		verifiedSeeds = verifiedSeeds.Where(str)
		countSeeds = countSeeds.Where(str)
	}

	err = countSeeds.RunWith(db.Conn).QueryRow().Scan(&sr.Total)
	if err != nil {
		log.Println(err)
		return
	}

	if sr.Total < 1 {
		return
	}

	for _, sort := range fs.Sort {
		if len(sort.Direction) < 1 || len(sort.Field) < 1 {
			continue
		}
		verifiedSeeds = verifiedSeeds.OrderBy(fmt.Sprintf("%s %s", sort.Field, strings.ToUpper(sort.Direction)))
	}

	offset := page * limit
	verifiedSeeds = verifiedSeeds.Limit(limit)
	verifiedSeeds = verifiedSeeds.Offset(offset)

	log.Println(verifiedSeeds.ToSql())

	rows, err := verifiedSeeds.RunWith(db.Conn).Query()
	if err != nil {
		log.Println(err)
		return
	}
	seeds, err := rowsToSeeds(rows)
	if err != nil {
		log.Println(err)
		return
	}
	sr.Seeds = seeds
	return
}

func (db DB) GetNotVerifiedSeeds(offset, limit string) (seeds []util.Seed, err error) {
	rows, err := db.Conn.Query("SELECT "+seedData+" FROM seeds WHERE verified = FALSE LIMIT $1 OFFSET $2;", limit, offset)
	if err != nil {
		return
	}
	seeds, err = rowsToSeeds(rows)
	if err != nil {
		log.Println(err)
	}
	return
}

func (db DB) GetSeed(seedID int64) (seed util.Seed, err error) {
	row := db.Conn.QueryRow("SELECT "+seedData+" FROM seeds WHERE seed = $1;", seedID)
	if err != nil {
		return
	}
	seed, err = rowToSeed(row)
	if err != nil {
		log.Println(err)
	}
	return
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func (db DB) RegisterSystemKey() (key string, err error) {
	key = RandStringRunes(30)
	result, err := db.Conn.Exec("INSERT INTO searchers (key) VALUES ($1);", key)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	return
}
